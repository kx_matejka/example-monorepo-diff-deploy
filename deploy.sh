#!/bin/bash
set -e

CHANGED_FILES=$(git diff-tree --no-commit-id --name-only -r $CI_COMMIT_ID | grep -c -E "^packages/${PACKAGE_NAME}/" | cat)

if [ "$CHANGED_FILES" -gt 0 ]
then
  echo "Something changed."
  npm run test
  npm run deploy
else
  echo "Nothing to do."
fi
